\section{State-of-the-art, Needs and Challenges}
\subsection{The need for privacy}
The scale of revenue earned from search engines is often hard to fathom.
As of 2021, Google processes 3.6 billion searches in a day alone.\cite{google-stats}
The proceeds from these searches are made exclusively from advertising, mainly
by personalizing results and promoting specific results through the use of
the Google Ads program.
Most search engines, like Bing, Yahoo, Badoo
employ similar methods for monetising searches.

\begin{figure}[h]
    \centering
    \begin{tikzpicture}
        \node (left) at (0,1)   {User};
        \node (middle) at (6,1) {Mainstream};
        \node (middledown) at (6,0.5) {Search Engine};
        \node (right) at (12,1) {Advertisement};
        \node (rightdown) at (12,0.5) {personalization};
        \node (rightdowndown) at (12,0) {system(s)};
        \draw[->,orange] (left.north)       .. controls +(up:2cm)   and +(left:1cm)    .. node[above,sloped] {\normalsize Queries} (middle.west);
        \draw[->,blue] (middle.north)       .. controls +(up:2cm)   and +(left:1cm)    .. node[above,sloped] {\normalsize Interests, Other user Data} (right.west);
        \draw[->,blue] (middledown.south)   .. controls +(down:2cm) and +(right:1.2cm) .. node[below,sloped] {\normalsize Results} (left.east);
        \draw[->,red] (rightdowndown.south) .. controls +(down:4cm) and +(right:1cm)   .. node[below,sloped] {\normalsize \ \ \ \ \ \ \ ,Targeted Advertisements} (left.east);
    \end{tikzpicture}
    \caption{\footnotesize{How personalized ads work in search engines, in simple terms.}}
    \label{fig:searx-oper}
\end{figure}

The issue that an increasing number of privacy conscious users and programmers
put forth in latest years is that, by personalizing searches and constructing
user profiles, an alarming amount of otherwise private information is given
away to third-party companies.
Such info can range from simple interests, for
example being a fan of a certain football club, to more sensitive details of
ones personal life, like medical issues and political stances.

These details are unfortunately proven many times to not be kept as private as
they are expected.
A significant incident, among many others, was the PRISM program, a top-secret
NSA operation that tapped into major tech companies' user information;
companies like Apple, Google and Facebook.\cite{prism}
Information regarding PRISM's purpose were uncovered chiefly by the
Guardian US\cite{guardian}, whose articles on secret
surveillance programs, including articles on the now notorious NSA whistleblower
Edward Snowden, crowned the publication the as the winner of the 2014
Pulitzer Prize in Public Service.\cite{pulitzer-guardian}
Other major, purely financial-driven, data breaches have surfaced over
the years too.
For example, the transfer of 86 million users' personally
identifiable data, without their consent, from Facebook to Cambridge Analytica,
a now defunct British political consulting firm.
The data breach sparked international debate, turning into a major scandal,
especially due to the fact that the data in question was used primarily to
aid in targeted political advertisements.
Facebook was forced to pay record-breaking fines, including a 5 billion dollar
fine from the Federal Trade Commission of the US Government\cite{5-billion}
and a 100 million dollar fine from the US. Securities and Exchange
Commission.\cite{100-million}
Cambridge Analytica, on the other hand, filed for insolvency during the
scandal in 2017.
However, related or successor firms to Cambridge Analytica,
like Emerdata Limited, still operate to this day.

A significant amount of social media users had their trust for online personal
data handling broken, leading to an even larger amount of people willing to
support modifications in national and worldwide legislation to prevent breaches.
Professionals in the field of Information published studies with detailed
proposals for preventative and protective laws for users, such as;
mandatory questions for consent in user data, compulsory adherence to the right
of the absence of tracking if users do not consent, the ability to identify,
terminate, delete, and uninstall any content or applications placed on their
devices or cloud service.\cite{ieee-privacy}

Partly as a result of the awareness raised by the previous events,
laws have been passed fairly recently to prevent misuse of user
information.
One characteristic example is the European Union's GDPR data
protection law, put into effect on May 25, 2018\cite{gdpr}.
This is notable, since it displays the transition of the growing distrust
about private user data handling, into a mainstream issue,
rather than just a concern from a computer-science knowledgeable minority.

Even since 2012, a study showed that 73\% of participants in a survey relating
to search engines did not have a favourable view of personalized ads on
search engines, while 68\% were not in favour of targeted advertisements at all,
viewing online behavioral tracking and analysing as unwanted.\cite{use-2012}

In current day, in spite of these new regulations, trust the average person
has for the handling of their personal information is at an all-time low,
since data leaks and security breaches are now announced on an almost regular
basis.
The need for transparency and thus open source software is now readily apparent.

\subsection{Current Technologies}

\subsubsection{Alternative and "Meta" Search Engines, the importance of crawlers}
In response to the aforementioned concerns, 
alternative search engines like DuckDuckGo\cite{duck} and Startpage
\cite{startpage} have grown in popularity, with the premise that their
services are more wary of users private info.
Specifically,
DuckDuckGo is currently the leading privacy-conscious search engine on the
internet, with its popularity surging in recent years and with the project
becoming more and more open source\cite{duck-open}.
Its traffic is indicative of the very recent spike in privacy-conscious software,
having grown by tens of millions of average daily search queries since 2020.\cite{duck-pop}
DuckDuckGo is unique, because unlike other projects of similar nature, it has
grown in popularity enough to have the resources to create its own web crawler,
which is no small task.

A web crawler is a program that automatically searches the internet and indexes
every website that meets certain requirements, for the purpose of these websites
to be able to be accurately searched and accessed from a search engine.
When used by a general internet search engine, the program, optimally,
needs to scan the entirety of the accessible world wide web, meaning millions of
websites to be processed and indexed per day.

\begin{figure}[h]
    \centering
    \begin{tikzpicture}
        \node (right)         at (14,4)   {World};
        \node (rightdown)     at (14,3.5) {Wide Web};
        \node (middle)        at (7,3)    {Downloader};
        \node (middledown)    at (7,0)    {Storage};
        \node (leftabove)     at (0,4)    {Processing};
        \node (leftabovedown) at (0,3.5)  {System};
        \draw[->,orange] (middle.north)        .. controls +(up:2cm)   and +(left:1cm)  .. node[above,sloped] {\small Search for Web pages \normalsize} (right.west);
        \draw[->,orange] (middle.north)        .. controls +(up:2cm)   and +(right:1cm) .. node[above,sloped] {\small URLs                 \normalsize} (leftabove.east);
        \draw[->,blue]   (rightdown.south)     .. controls +(down:2cm) and +(right:1cm) .. node[below,sloped] {\small Found Web pages      \normalsize} (middle.east);
        \draw[->,red]    (leftabovedown.south) .. controls +(down:2cm) and +(left:1cm)  .. node[below,sloped] {\small Ordered URLs         \normalsize} (middle.west);
        \draw[->,brown]  (middle.south) to node[right,near end] {Text and Metadata} (middledown.north);
    \end{tikzpicture}
    \caption{\footnotesize{The anatomy of a web crawler, in simple terms.}}
    \label{fig:anatomy-crawler}
\end{figure}

Largely due to this fact, general purpose open source web search engines are
still few and far between.
As Sergey Brin and Lawrence Page, in their paper:\emph{
The Anatomy of a Large-Scale Hypertextual Web Search Engine},
presenting their, at the time, new search engine, "Google", pointed out:
\\
\begin{addmargin}[2em]{3em}
\emph{"Running a web crawler is a challenging task.
There are tricky
performance and reliability issues and even more importantly, there are social
issues.
Crawling is the most fragile application since it involves interacting
with hundreds of thousands of web servers and various name servers which are
all beyond the control of the system.}\cite{google}
\\
\end{addmargin}
This statement is still true.
Fully open source projects seem to have a hard
time making crawlers that scan the entirety of the internet with the accuracy
users have come to expect from mainstream competitors.
Most mainstream search
engines maintain codebases that have been refined for many years and with
marginally greater financial assets.

To overcome the lack of infrastructure, "metasearch" engines were created.
These search engines frequently lack their own crawlers, instead
opting to use search results from other engines to handle user queries.
Without the need to maintain and develop a search engine from the ground up,
the maintenance of this software consists mostly in configuring host servers,
ensuring uninterrupted and private communication between the system and
the target search
engines, adding features like previews, updating the user interface, etc.
Previously mentioned Netherlands-based search engine Startpage uses Google's
search results, with the significant difference that search queries are first
passed through Startpages' servers and reaching Google by proxy, effectively
circumventing Google's advertisement personalisation and tracking.
While currently
Startpage is not open source, more metasearch engines are created with a
similar philosophy.
\subsubsection{The example of Searx, a Metasearch Engine}
A prime example of a fully open source metasearch engine is Searx\cite{searx}.
Searx is a search engine, programmed mainly in Python\cite{python}, that
aggregates multiple search engine results based on a single user query.\ It is
subject to the GNU Affero General Public License version 3.0\cite{AGPLv3},
making its source code freely available to the public via the project's GitHub
repository.
Users can even host their own instance of a Searx server which
other users can pass through their own search queries.
\begin{figure}[h]
    \centering
\begin{tikzpicture}
    \node (left) at (0,0)   {User};
    \node (middle) at (6,0) {Searx\ Instance};
    \node (right) at (12,0) {Search Engine(s)};

    \draw[->,orange] (left.north) .. controls +(up:2cm) and +(left:1cm) .. node[above,sloped] {\scriptsize Step 1: \normalsize Query} (middle.west);
    \draw[->,blue] (middle.north) .. controls +(up:2cm) and +(left:1cm) .. node[above,sloped] {\scriptsize Step 2: \normalsize User's Query} (right.north);

    \draw[->,red] (right.south)   .. controls +(down:2cm) and +(right:1cm) .. node[below,sloped] {\scriptsize Step 3: \normalsize Results} (middle.east);
    \draw[->,blue] (middle.south) .. controls +(down:2cm) and +(right:1cm) .. node[below,sloped] {\scriptsize Step 4: \normalsize Search Engine's Results} (left.south);
\end{tikzpicture}
    \caption{\footnotesize{How Searx operates, in 4 simple steps.}}
\label{fig:searx-oper}
\end{figure}

Searx is software that runs on a server.
One server can host any number of "Searx instances", which are Searx
processes that are publicly available\cite{space} and running on the server.
These instances can be used by any member of the public who visit their URL.
When search queries by users are inserted into an instance by, for example,
typing on the instance's website, the process passes their queries into multiple
search engines.
These are most commonly Google, DuckDuckGo, but they could be even more, up to
70 search engines, with some configuration.
When the selected search engines respond to the Searx instance with the results,
the instance returns them to the user through its website all at once, thus
aggregating them.

This method has benefits for the privacy conscious:
\begin{itemize}
    \item Search result and Advertisement personalisation is circumvented.
          Each Searx instance, if public, is not used by a single user.
          Instances are also frequently set up to access search engines through
          a VPN or a proxy, ensuring the privacy of the users further.
    \item An instance could be running a fork of the Searx project, which is
          a modified version of the original source code, adding specific
          features.
    \item The code every instance is running must be publicly available, making
          inspection of source code easy, as way to detect malicious behavior.
\end{itemize}

\subsubsection{A few words about Small Scale Search Engines}
There exists one area of Information Recovery on the internet where open source
search engines comprise the majority of used search tools.
This area is none other than local search engines on websites.
Developers favour open source search engines that can be found online without
charge and freely altered to the particular website's needs and purposes.
These search engines, like metasearch engines, also lack their own crawlers,
with the key difference that they do not use crawlers at all.
Simple queries to the website's database are commonly implemented instead.
Notable and popular examples of search engines that are used to peruse
information on sites, instead of the entirety of the web, are Apache Lucene\cite{Lucene}
and Typesense\cite{Typesense}.

\subsection{What lies ahead}\label{subsec:what-lies-ahead}
\subsubsection{Uncertainty in the field of Information Recovery}
While the future of open source software in the gradually industrialized
field of programming seems to be bright, what lies ahead for open
source search engines is unsure at best.
Open source search engines do not operate with profits as their primary concern.
Especially metasearch engines, who aggregate results from different, most times
closed source search engines, run by for-profit companies, face significant
difficulties.
These are often hard to combat, since developers of the metasearch engines
do not have access to the code that yields the results, leading to a multitude
of issues being much harder to debug than normally expected.
\subsubsection{Obstacles of Technical Nature}
A good example is related to events since at least 2016, concerning Searx and
Google.
Users trying to connect through certain instances, popular or not, were
faced with a Google error informing them of a failed CAPTCHA\cite{captcha}.
This particular issue seem to stem from the fact that Google began to blacklist
IP of these Searx instances.
Further analysis in 2018 by other users revealed that Google seems to had made
their anti-bot filters more strict, resulting in detection of some third-party
bots that populated publicly available instances.
Since this reveal, users are advised to use other software to filter out
non-organic traffic from their instances, by making rules for accepting incoming
requests, with programs like Filtron.\cite{filtron}
Unfortunately, this issue began to resurface again, this time when Google
changed its filter to limit the amount of incoming HTTP1.1 requests onto their
servers.
Developers eventually solved most of the occurrences by making the code send the
less limited HTTP2.0 requests instead of the HTTP1.1 ones, but not until
2020.\cite{events}

These events really highlight the unexpected nature of the issues developers
encounter while maintaining a seemingly simple open source search engine
project.
The true scope of this kind of software is occasionally underestimated by the
public.
Tasks in programming are often more challenging than they appear at first.
In reality, when the project's purpose is critical and its user-base
magnifies in size, so do the obstacles developers face.
Some of these obstacles programmers could never realistically predict beforehand.

\subsubsection{Obstacles of Societal Nature}
For the privacy-conscious, open source search engines seem to be one of the most
thorough methods to ensure the privacy of their search queries and the
information they provide.

However, it also seems that the amount of technicality these intricate pieces
of software have is more than likely to overwhelm a user that is not
significantly technologically proficient, or is not willing to try to be.
The use of these engines might appear to be user-friendly and non-technical at
first, but to really be able to reap the benefits of online privacy as a whole,
there exists a surprising amount of knowledge one must acquire, about more
concepts than just search engine personalisation.

Furthermore, as privacy measures are invented, so are surveillance,
personalisation and monitoring tools.
Since financial incentive for the vast majority of actors in computing
is not user-privacy but rather personalisation and
advertising optimization, the struggle to ensure even a moderate amount of
online privacy is; and will be, constant.
Technologies rapidly evolve, so the knowledge base also grows exponentially.
This means users are forced to learn and research even more, progressively
setting the standards even higher for new users, making the task of keeping up
with recent developments even harder.

Fortunately, studies have shown that the amount of digital natives, meaning people
that are exposed to technology and are computer-literate from a relatively young
age, has grown in the latest years, drawing a direct correlation of age with
computer literacy.
For example, a 2018 study of the Organisation for Economic Cooperation and
Development (OECD) claims that, in the majority of OECD countries
(38 countries)\cite{list-oecd},
nearly all 16-24 year-olds use the Internet, with a median value of 96\%,
while 55-74 year-olds had a median of 55\%.\cite{oecd}
Newer generations are more familiar with the internet, which
is hopeful for the increase of awareness about societal issues that rise from
widespread technology usage.
In these particular issues, internet privacy is one of the most prominent.
It is not unreasonable to suggest that even more widespread awareness on the
topic is inevitable; not to mention imperative, in future years.